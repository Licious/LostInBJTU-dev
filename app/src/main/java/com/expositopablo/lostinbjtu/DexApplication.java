package com.expositopablo.lostinbjtu;

import android.support.multidex.MultiDexApplication;

/**
 * Created by pablo on 08/05/2017 for Lostinbjtu.
 */

public class DexApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
