package com.expositopablo.lostinbjtu;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.expositopablo.lostinbjtu.database.DBManager;
import com.expositopablo.lostinbjtu.models.LocationModel;
import com.expositopablo.lostinbjtu.models.googlemap.DirectionModel;
import com.expositopablo.lostinbjtu.models.googlemap.LegModel;
import com.expositopablo.lostinbjtu.models.googlemap.RouteModel;
import com.expositopablo.lostinbjtu.models.googlemap.StepModel;
import com.expositopablo.lostinbjtu.network.GoogleMapAPIService;
import com.expositopablo.lostinbjtu.network.LocationNetworkService;
import com.expositopablo.lostinbjtu.network.ServiceFactory;
import com.expositopablo.lostinbjtu.utilities.BitmapUtilities;
import com.expositopablo.lostinbjtu.views.EventInfoActivity;
import com.expositopablo.lostinbjtu.views.EventListActivity;
import com.expositopablo.lostinbjtu.views.LocationInfoActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.messaging.FirebaseMessaging;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Cancellable;
import io.reactivex.schedulers.Schedulers;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, View.OnClickListener {

    private static final String DBCREATED = "isDBCreated";
    public static final int REQUEST_CODE = 101;
    private static final String TAG = "MapsActivity";
    private static final int PERMISSION_REQUEST_CODE = 0x1;


    @BindView(R.id.list)
    @Nullable
    RecyclerView list;
    @BindView(R.id.query_edit_text)
    @Nullable
    EditText queryEditText;
    @BindView(R.id.search_button)
    @Nullable
    Button searchButton;
    @BindView(R.id.floatingActionButton_maps_event)
    @Nullable
    FloatingActionButton fabEvent;
    @BindView(R.id.map)
    @Nullable
    View view;

    private Boolean isMapVisible = true;
    private GoogleMap mMap;
    private DBManager db;
    private ArrayList<LocationModel> locations = new ArrayList<>();
    private SupportMapFragment mapFragment;
    private ListLocationAdapter adapter;
    private LocationManager locationManager;
    private String provider;
    private MarkerOptions myself;
    private boolean providerStatus = false;
    private Location myLocation;
    private Disposable mDisposable;
    private LocationModel location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences.getBoolean(DBCREATED, false)) {
            startRequest();
            FirebaseMessaging.getInstance().subscribeToTopic("lostinbjtu");
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null)
            location = extras.getParcelable(EventInfoActivity.LOCATION);

        db = new DBManager(this);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new ListLocationAdapter(this, locations);
        list.setAdapter(adapter);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        provider = locationManager.getBestProvider(criteria, false);
        if (provider == null)
            Log.i(TAG, "onCreate : provider is null");
        else {
            Log.i(TAG, "onCreate : provider is : " + provider);
            providerStatus = true;
        }
        fabEvent.setOnClickListener(this);

    }

    /**
     * Start The onluen request to get the buildings from the API
     */
    public void startRequest() {
        LocationNetworkService network = ServiceFactory.createRetrofitService(LocationNetworkService.class, BuildConfig.SERVER_ENDPOINT);
        Single<List<LocationModel>> locationNetworkObservable = network.getLocations();
        locationNetworkObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locations -> {
                    if (locations != null) {
                        DBManager manager = DBManager.getDbManager(getApplicationContext());
                        manager.openForWrite();
                        locations.forEach(manager::addLocationModel);
                        manager.close();
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(DBCREATED, true);
                        editor.apply();
                    }
                }, throwable -> {
                    //Something Went Wrong
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(39.950788, 116.343841);
        LatLngBounds bounds = new LatLngBounds(new LatLng(39.942877, 116.337947), new LatLng(39.960774, 116.346530));

        mMap.setLatLngBoundsForCameraTarget(bounds);
        mMap.setMinZoomPreference(15.5f);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15.5f));

        updatePosition();

        if (location != null) {
            showLocation(location);
            location = null;
        }
    }

    /**
     * Create an Observable to observe TextChange on the searchBar
     * @return Observable with the String inside searchBar
     */
    private Observable<String> createTextChangeObservable() {
        Observable<String> textChangeObservable = Observable.create(emitter -> {
            final TextWatcher watcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    emitter.onNext(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            };

            queryEditText.addTextChangedListener(watcher);
            emitter.setCancellable(() -> queryEditText.removeTextChangedListener(watcher));
        });
        return textChangeObservable.filter(s -> s.length() >= 2).debounce(1000, TimeUnit.MILLISECONDS);
    }

    private Observable<String> createButtonClickObservable() {
        return Observable.create(emitter -> {
            searchButton.setOnClickListener(view1 -> emitter.onNext(queryEditText.getText().toString()));

            emitter.setCancellable(() -> searchButton.setOnClickListener(null));
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Observable<String> buttonClickStream = createButtonClickObservable();
        Observable<String> textChangeStream = createTextChangeObservable();
        Observable<String> searchTextObservable = Observable.merge(buttonClickStream, textChangeStream);

        mDisposable = searchTextObservable
                .observeOn(Schedulers.io())
                .doOnNext(s -> {
                })
                .observeOn(AndroidSchedulers.mainThread())
                .map(query -> {
                    isMapVisible = false;
                    view.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                    fabEvent.setVisibility(View.GONE);
                    ArrayList<LocationModel> locationList = new ArrayList<>();
                    db.openForRead();
                    locationList.addAll(db.getLocationByNameOrCode(query));
                    db.close();
                    locationList.add(new LocationModel(null, "SiYuan", "SY", "Main Campus", null, "Main building, in front of the big area of grass and chinese flag.", "Not a lot of plugs, bring yours", 39.951135, 116.340892, "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Bjtu_siyuan_building.JPG/280px-Bjtu_siyuan_building.JPG"));
                    locationList.add(new LocationModel(null, "YiFu Teaching Building", "YF", "Main Campus", null, "Main building of software engineering school", "Not enough plugs, brings yours", 0.0, 0.0, "http://google.fr/404"));
                    locationList.add(new LocationModel(null, "SiXi", "SX", "Main Campus", null,  "As indicated by the name, this building is located at the west of the Main Garden", "Not enough plugs, bring yours", 0.0, 0.0, null));
                    locationList.add(new LocationModel(null, "SiDong", "SD", "Main Campus", null, "As indicated by the name, this building is located at the east of the Main Garden", "Not enough plugs, bring yours", 0.0, 0.0, null));
                    locationList.add(new LocationModel(null, "Ming Lake", null, "Main Campus", null, "Best Spot of the university, lof of couples at night", "Bring your camera to catch the best pictures", 0.0, 0.0, null));
                    locationList.add(new LocationModel(null, "N°4 international Student Dormitory", null, "Main Campus", null,  "Full of french, might smell cheese", "Don't bring chinese girls here", 39.94990, 116.3434, null));
                    locationList.add(new LocationModel(null, "N°14 international Student Dormitory", null, "Main Campus", null, "Probably the best dormitory", "", 0.0, 0.0, null));
                    locationList.add(new LocationModel(null, "N°1 international Student Dormitory", null, "Main Campus", null, "Who's here?", "", 0.0, 0.0, null));
                    return locationList;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locationModels -> adapter.setLocations(locationModels));
    }

    @Override
    public void onBackPressed() {
        if (!isMapVisible) {
            hideList();
        } else
            finish();
    }

    /**
     * Hide the ListView And display map
     */
    public void hideList(){
        isMapVisible = true;
        view.setVisibility(View.VISIBLE);
        list.setVisibility(View.GONE);
        fabEvent.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check if the request code is same as what is passed  here it is 2
        if (requestCode == REQUEST_CODE) {
            if (data != null) {
                LocationModel model = data.getParcelableExtra("Return");
                showLocation(model);
            }
        }
    }

    /**
     * Show The location of the building and the way to get There when the user activate his GPS.
     * @param model Location object
     */
    protected void showLocation(LocationModel model){
        isMapVisible = true;
        hideList();
        if (mMap !=  null) {
            mMap.clear();
            myLocation = mMap.getMyLocation();
            mMap.addMarker(new MarkerOptions().position(new LatLng(model.getLatitude(), model.getLongitude())).title("Arrival")).setIcon(BitmapDescriptorFactory.fromBitmap(BitmapUtilities.getBitmapFromVectorDrawable(MapsActivity.this, R.drawable.pointer)));
        }
        if (myLocation != null) {
            GoogleMapAPIService service = ServiceFactory.createRetrofitService(GoogleMapAPIService.class, BuildConfig.MAP_API);

            Single<DirectionModel> path = service.getPath(myLocation.getLatitude() + "," + myLocation.getLongitude(), model.getLatitude() + "," + model.getLongitude(), false, "walking");
            path.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(direction -> {
                System.out.println(MapsActivity.TAG + " " + direction.getStatus());
                List<RouteModel> routes = direction.getRouteModels();
                int i = 0;
                for (RouteModel route : routes) {
                    List<LegModel> legs = route.getLegModels();
                    PolylineOptions rectOptions = new PolylineOptions();
                    for (LegModel leg : legs) {
                        LatLng departure = new LatLng(leg.getStartLocation().getLat(), leg.getStartLocation().getLng());
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(departure, 16.0f));
                        //mMap.addMarker(new MarkerOptions().position(departure).title("Departure")).setIcon(BitmapDescriptorFactory.fromBitmap(BitmapUtilities.getBitmapFromVectorDrawable(MapsActivity.this, R.drawable.pointer)));
                        List<StepModel> steps = leg.getStepModels();
                        for (StepModel step : steps) {
                            rectOptions.add(new LatLng(step.getStartLocation().getLat(), step.getStartLocation().getLng()));
                            rectOptions.add(new LatLng(step.getEndLocation().getLat(), step.getEndLocation().getLng()));
                        }
                    }
                    if (i < 1)
                        mMap.addPolyline(rectOptions.color(Color.BLUE));
                    else
                        mMap.addPolyline(rectOptions.color(Color.GRAY));
                    i++;
                }
            }, throwable -> {
            });
        }else{
            Log.i(TAG, "onActivityResult : myLocation is null");
        }
    }

    /* Request updates at startup */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "onLocationChanged");
        myLocation = location;
//        if (location != null)
//        myself = new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Arrival");
//        mMap.clear();
//        mMap.addMarker(myself).setIcon(BitmapDescriptorFactory.fromBitmap(BitmapUtilities.getBitmapFromVectorDrawable(MapsActivity.this, R.drawable.goal)));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
        Toast.makeText(this, "onStatusChanged " + provider,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();
        if (provider != null && provider.equals("gps")) {
            providerStatus = true;
            updatePosition();
        }
    }

    /**
     * update The Position of the user on the map.
     */
    private void updatePosition() {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("CoarseLocation");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        (dialog, which) -> {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        PERMISSION_REQUEST_CODE);
                            }
                        });
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        PERMISSION_REQUEST_CODE);
            }
            return;
        }
        if (providerStatus && mMap != null) {
            Log.i(TAG, "Going to Ask for Update gps Provider");
            locationManager.requestLocationUpdates(provider, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            mMap.setMyLocationEnabled(true);
            myLocation = mMap.getMyLocation();
//            mMap.getMyLocation();
            //locationManager.requestLocationUpdates(Manifest.permission.ACCESS_COARSE_LOCATION, 0, 0, this);
        }
    }

    /**
     * Method to check and Add Permission at RunTime.
     * @param permissionsList List of application's permissions
     * @param permission permission to check and Add at runTime
     * @return
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    /**
     * Dialog window with custom message.
     * @param message Message to Display
     * @param okListener Click Listener
     */
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MapsActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
        if (provider != null && provider.equals("gps"))
            providerStatus = false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    updatePosition();
                } else {
                    // Permission Denied
                    Toast.makeText(MapsActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.floatingActionButton_maps_event:
                Intent intent = new Intent(this, EventListActivity.class);
                startActivity(intent);
                break;
            default:
        }
    }
}

/**
 * Custom Class Adapter to Display location carview on RecyclerView
 */
class ListLocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /** Context of the activity */
    private final Context context;
    /** List of location */
    private ArrayList<LocationModel> locations;

    /**
     * Default constructor
     * @param context Application's context
     * @param locations List of location to display
     */
    public ListLocationAdapter(Context context, ArrayList<LocationModel> locations) {
        this.context = context;
        this.locations = locations;
    }

    /**
     * Creation of ViewHolder
     * @param parent Parent View
     * @param viewType View to display in the cardView
     * @return ViewHolder that contain the card View
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolderDetail(LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_detail, parent, false), context);
    }

    /**
     * Bind the location model to the View and display Information
     * @param holder ViewHolder that keep the View
     * @param position Position of the card in the RecyclerView
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolderDetail) holder).bind(locations.get(position));
    }

    /**
     *
     * @return Number of element in the list
     */
    @Override
    public int getItemCount() {
        return (locations != null)? locations.size() : 0;
    }

    /**
     * Setter for the location List
     * @param locations List of location
     */
    public void setLocations(ArrayList<LocationModel> locations) {
        this.locations = locations;
        notifyDataSetChanged();
    }

    /**
     * Inner class of Custom Adapter. Viewholder that contain View of the child element.
     */
    private class ViewHolderDetail extends RecyclerView.ViewHolder {
        /** Application Context */
        Context context;
        TextView go;
        TextView code;
        TextView title;

        /**
         * Default Constructor
         * @param view View to display in the list
         * @param context Application Context
         */
        ViewHolderDetail(View view, Context context) {
            super(view);
            this.context = context;
            go = (TextView) view.findViewById(R.id.textView_locationListDetail_go);
            code = (TextView) view.findViewById(R.id.textView_locationListDetail_code);
            title = (TextView) view.findViewById(R.id.textView_locationListDetail_title);
        }

        /**
         * Bind location model to the view
         * @param location Location Object. Contain all the information to display in the view.
         */
        public void bind(final LocationModel location) {
            code.setText(location.getCode());
            title.setText(location.getTitle());
            title.setOnClickListener(view -> {
                Intent intent = new Intent(context, LocationInfoActivity.class);
                intent.putExtra("location", location);
                ((MapsActivity) context).startActivityForResult(intent, MapsActivity.REQUEST_CODE);
            });
            code.setOnClickListener(view -> {
                Intent intent = new Intent(context, LocationInfoActivity.class);
                intent.putExtra("location", location);
                ((MapsActivity) context).startActivityForResult(intent, MapsActivity.REQUEST_CODE);
            });
            go.setOnClickListener(view -> ((MapsActivity)context).showLocation(location));
        }
    }
}
