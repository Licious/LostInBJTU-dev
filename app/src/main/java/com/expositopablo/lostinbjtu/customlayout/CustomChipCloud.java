/*
 * MIT License
 *
 * Copyright (c) 2017 Exposito Pablo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.expositopablo.lostinbjtu.customlayout;

import android.content.Context;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;

public class CustomChipCloud extends ChipCloud {

    private final WeakReference<Context> context;
    private final ViewGroup layout;
    private ChipCloudConfig config = null;

    public CustomChipCloud(Context context, ViewGroup layout, ChipCloudConfig config) {
        super(context, layout, config);

        this.context = new WeakReference<>(context);
        this.layout = layout;
        this.config = config;
    }

    // For removing all the ToggleChips from the view & add new
    public void refresh() {
        layout.removeAllViews();
    }
}
