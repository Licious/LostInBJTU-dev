package com.expositopablo.lostinbjtu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.expositopablo.lostinbjtu.models.LocationModel;

import java.util.ArrayList;

public class DBManager {

    //region Attributes
    private SQLiteDatabase bdd;
    private DatabaseHelper  dbHelper;
    private static String   TAG = "DBManager";
    private static DBManager DBMANAGER = null;
    //endregion

    //region Constructor
    public DBManager(Context context){
        dbHelper = new DatabaseHelper(context);
    }

    public static DBManager getDbManager(Context context){
        if (DBMANAGER ==  null)
            DBMANAGER = new DBManager(context);
        return DBMANAGER;
    }
    //endregion

    //region Open Read and Close
    public void openForRead(){
        bdd = dbHelper.getReadableDatabase();
    }

    public void openForWrite(){
        bdd = dbHelper.getWritableDatabase();
    }

    public void close(){
        bdd.close();
    }
    //endregion
    public void printTables(){
        Cursor c = dbHelper.showAllTables(bdd);
        ArrayList<String> tables = new ArrayList<>();
        if (c.moveToFirst())
        {
            do{
                tables.add(c.getString(0));

            }while (c.moveToNext());
        }
        Log.d("PrintTables", "TODOItems Size = " + tables.size());
        if (tables.size() >= 0)
        {
            for (int i=0; i<tables.size(); i++)
            {
                Log.d("TODOItems(" + i + ")", tables.get(i) + "");

            }

        }
    }

    //Region Getter
    public SQLiteDatabase getBdd(){
        return bdd;
    }
    //endregion

    public String addLocationModel(LocationModel model){
        ContentValues content = new ContentValues();
        content.put(DatabaseHelper.ID, model.getId());
        content.put(DatabaseHelper.TITLE, model.getTitle());
        content.put(DatabaseHelper.CODE, model.getCode());
        content.put(DatabaseHelper.CAMPUS, model.getCampus());
        content.put(DatabaseHelper.ALTERNATIVE, model.getAlternative());
        content.put(DatabaseHelper.DESCRIPTION, model.getDescription());
        content.put(DatabaseHelper.ADVICE, model.getAdvice());
        content.put(DatabaseHelper.IMAGELINK, model.getImageLink());
        content.put(DatabaseHelper.LONGITUDE, model.getLongitude());
        content.put(DatabaseHelper.LATITUDE, model.getLatitude());
        bdd.insert(DatabaseHelper.LOCATION_TABLE, null, content);
        return model.getId();
    }

    public int updateLocationModel(String id, LocationModel model){
        ContentValues content = new ContentValues();
        content.put(DatabaseHelper.ID, id);
        content.put(DatabaseHelper.TITLE, model.getTitle());
        content.put(DatabaseHelper.CODE, model.getCode());
        content.put(DatabaseHelper.CAMPUS, model.getCampus());
        content.put(DatabaseHelper.ALTERNATIVE, model.getAlternative());
        content.put(DatabaseHelper.DESCRIPTION, model.getDescription());
        content.put(DatabaseHelper.ADVICE, model.getAdvice());
        content.put(DatabaseHelper.IMAGELINK, model.getImageLink());
        content.put(DatabaseHelper.LONGITUDE, model.getLongitude());
        content.put(DatabaseHelper.LATITUDE, model.getLatitude());
        bdd.insert(DatabaseHelper.LOCATION_TABLE, null, content);

        return bdd.update(DatabaseHelper.LOCATION_TABLE, content, DatabaseHelper.ID + " = '" + id + "'", null);
    }

    public ArrayList<LocationModel> getAllLocations(){
        ArrayList<LocationModel> models = new ArrayList<>();
        Cursor cursor = bdd.query(
                DatabaseHelper.LOCATION_TABLE,
                null,
                null,
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            while(cursor.moveToNext()) {
                LocationModel locationModel = new LocationModel(
                        cursor.getString(DatabaseHelper.ID_COL),
                        cursor.getString(DatabaseHelper.TITLE_COL),
                        cursor.getString(DatabaseHelper.CODE_COL),
                        cursor.getString(DatabaseHelper.CAMPUS_COL),
                        cursor.getString(DatabaseHelper.ALTERNATIVE_COL),
                        cursor.getString(DatabaseHelper.DESCRIPTION_COL),
                        cursor.getString(DatabaseHelper.ADVICE_COL),
                        cursor.getDouble(DatabaseHelper.LONGITUDE_COL),
                        cursor.getDouble(DatabaseHelper.LATITUDE_COL),
                        cursor.getString(DatabaseHelper.IMAGELINK_COL));
                models.add(locationModel);
            }
        }
        cursor.close();
        return models;
    }

    public LocationModel getLocationByID(String id){
        String whereClause = DatabaseHelper.ID + " = '" + id + "'";

        Cursor cursor = bdd.query(
                DatabaseHelper.LOCATION_TABLE,
                null,
                whereClause,
                null,
                null,
                null,
                null);

        LocationModel locationModel = null;
        if (cursor.getCount() != 0 && cursor.moveToNext()) {
            locationModel = new LocationModel(
                    cursor.getString(DatabaseHelper.ID_COL),
                    cursor.getString(DatabaseHelper.TITLE_COL),
                    cursor.getString(DatabaseHelper.CODE_COL),
                    cursor.getString(DatabaseHelper.CAMPUS_COL),
                    cursor.getString(DatabaseHelper.ALTERNATIVE_COL),
                    cursor.getString(DatabaseHelper.DESCRIPTION_COL),
                    cursor.getString(DatabaseHelper.ADVICE_COL),
                    cursor.getDouble(DatabaseHelper.LONGITUDE_COL),
                    cursor.getDouble(DatabaseHelper.LATITUDE_COL),
                    cursor.getString(DatabaseHelper.IMAGELINK_COL));
        }
        cursor.close();
        return locationModel;
    }

    public ArrayList<LocationModel> getLocationByNameOrCode(String toSearch){
        String whereClause = DatabaseHelper.TITLE + " LIKE  '%" + toSearch + "%' OR " + DatabaseHelper.CODE + " LIKE '%" + toSearch + "%'";

        Cursor cursor = bdd.query(
                DatabaseHelper.LOCATION_TABLE,
                null,
                whereClause,
                null,
                null,
                null,
                null);
        ArrayList<LocationModel> models = new ArrayList<>();
        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                LocationModel  locationModel = new LocationModel(
                        cursor.getString(DatabaseHelper.ID_COL),
                        cursor.getString(DatabaseHelper.TITLE_COL),
                        cursor.getString(DatabaseHelper.CODE_COL),
                        cursor.getString(DatabaseHelper.CAMPUS_COL),
                        cursor.getString(DatabaseHelper.ALTERNATIVE_COL),
                        cursor.getString(DatabaseHelper.DESCRIPTION_COL),
                        cursor.getString(DatabaseHelper.ADVICE_COL),
                        cursor.getDouble(DatabaseHelper.LONGITUDE_COL),
                        cursor.getDouble(DatabaseHelper.LATITUDE_COL),
                        cursor.getString(DatabaseHelper.IMAGELINK_COL));
                models.add(locationModel);
            }
        }
        cursor.close();
        return models;
    }

    public int countLocation(){
        return countRow(DatabaseHelper.LOCATION_TABLE);
    }

    public int deleteLocation(String id){
        return deleteRow(DatabaseHelper.LOCATION_TABLE, id);
    }

    private int deleteRow(String id, String table){
        return bdd.delete(table, DatabaseHelper.ID + " = '" + id + "'", null);
    }

    private int countRow(String table){
        Cursor cursor = bdd.query(table, new String[]{DatabaseHelper.ID}, null, null, null, null, null);
        int nb = cursor.getCount();
        cursor.close();
        return nb;
    }
}
