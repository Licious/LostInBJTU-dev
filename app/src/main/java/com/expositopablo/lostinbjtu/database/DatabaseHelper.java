package com.expositopablo.lostinbjtu.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "lostinBJTU.db";
    private static final int VERSION = 1;

    public static final String LOCATION_TABLE = "locations";
    public static final String ID = "id";
    public static final int ID_COL = 0;
    public static final String TITLE = "title";
    public static final int TITLE_COL = 1;
    public static final String CODE = "code";
    public static final int CODE_COL = 2;
    public static final String CAMPUS = "campus";
    public static final int CAMPUS_COL = 3;
    public static final String ALTERNATIVE = "alternative";
    public static final int ALTERNATIVE_COL = 4;
    public static final String DESCRIPTION = "description";
    public static final int DESCRIPTION_COL = 5;
    public static final String ADVICE = "advice";
    public static final int ADVICE_COL = 6;
    public static final String IMAGELINK = "imageLink";
    public static final int IMAGELINK_COL = 7;
    public static final String LONGITUDE = "longitude";
    public static final int LONGITUDE_COL = 8;
    public static final String LATITUDE = "latitude";
    public static final int LATITUDE_COL = 9;

    private static final String CREATE_LOCALISATION_TABLE =
            "CREATE TABLE IF NOT EXISTS " + LOCATION_TABLE + "( "
            + ID + " TEXT PRIMARY KEY NOT NULL, "
            + TITLE + " TEXT, "
            + CODE + " TEXT, "
            + CAMPUS + " TEXT, "
            + ALTERNATIVE + " TEXT, "
            + DESCRIPTION + " TEXT, "
            + ADVICE + " TEXT, "
            + IMAGELINK + " TEXT, "
            + LONGITUDE + " DOUBLE, "
            + LATITUDE + " DOUBLE );";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createTables(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private void createTables(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL(CREATE_LOCALISATION_TABLE);
    }

    private void dropTables(SQLiteDatabase sqLiteDatabase){
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LOCATION_TABLE);
    }

    /**
     * For Debug Purpose
     *
     * @param sqLiteDatabase SQliteDatabase
     * @return Cursor
     */
    public Cursor showAllTables(SQLiteDatabase sqLiteDatabase){
        String mySql = " SELECT name FROM sqlite_master " + " WHERE type='table'";
        return sqLiteDatabase.rawQuery(mySql, null);
    }
}
