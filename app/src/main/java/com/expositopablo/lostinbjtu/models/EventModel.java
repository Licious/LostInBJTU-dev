package com.expositopablo.lostinbjtu.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;
import java.util.List;

@JsonPropertyOrder({
        "name",
        "date",
        "location",
        "description",
        "image",
        "imagethumbs",
        "categories",
        "mail",
        "tel"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventModel implements Parcelable {

    @JsonProperty("name")
    private String title;
    @JsonProperty("date")
    private Date date;
    @JsonProperty("categories")
    private List<String> categories;
    @JsonProperty("description")
    private String description;
    @JsonProperty("image")
    private String image;
    @JsonProperty("imagethumbs")
    private String imageThumbs;
    @JsonProperty("mail")
    private String mail;
    @JsonProperty("tel")
    private String tel;
    @JsonProperty("location")
    private LocationModel location;

    public EventModel() {
    }

    public EventModel(String title, Date date, List<String> categories, String description, String image, String imageThumbs, String mail, String tel, LocationModel location) {
        this.title = title;
        this.date = date;
        this.categories = categories;
        this.description = description;
        this.image = image;
        this.imageThumbs = imageThumbs;
        this.mail = mail;
        this.tel = tel;
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageThumbs() {
        return imageThumbs;
    }

    public void setImageThumbs(String imageThumbs) {
        this.imageThumbs = imageThumbs;
    }

    public LocationModel getLocation() {
        return location;
    }

    public void setLocation(LocationModel location) {
        this.location = location;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeLong(this.date != null ? this.date.getTime() : -1);
        dest.writeStringList(this.categories);
        dest.writeString(this.description);
        dest.writeString(this.image);
        dest.writeString(this.imageThumbs);
        dest.writeString(this.mail);
        dest.writeString(this.tel);
        dest.writeParcelable(this.location, flags);
    }

    protected EventModel(Parcel in) {
        this.title = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.categories = in.createStringArrayList();
        this.description = in.readString();
        this.image = in.readString();
        this.imageThumbs = in.readString();
        this.mail = in.readString();
        this.tel = in.readString();
        this.location = in.readParcelable(LocationModel.class.getClassLoader());
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel source) {
            return new EventModel(source);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };
}
