package com.expositopablo.lostinbjtu.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
        "_id",
        "name",
        "alternative",
        "code",
        "campus",
        "longitude",
        "latitude",
        "image",
        "description",
        "advice"
})
public class LocationModel implements Parcelable {

    //region attributs
    @JsonProperty("_id")
    private String id;
    @JsonProperty("name")
    private String title;
    @JsonProperty("code")
    private String code;
    @JsonProperty("campus")
    private String campus;
    @JsonProperty("alternative")
    private String alternative;
    @JsonProperty("description")
    private String description;
    @JsonProperty("advice")
    private String advice;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("image")
    private String imageLink;
    //endregion

    //region Constructors
    public LocationModel(String id, String title, String code, String campus, String alternative, String description, String advice, Double latitude, Double longitude, String imageLink) {
        this.id = id;
        this.title = title;
        this.code = code;
        this.campus = campus;
        this.alternative = alternative;
        this.description = description;
        this.advice = advice;
        this.longitude = longitude;
        this.latitude = latitude;
        this.imageLink = imageLink;
    }
    //endregion

    //region Getter && Setter
    @JsonProperty("_id")
    public String getId() {
        return id;
    }

    @JsonProperty("_id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getTitle() {
        return title;
    }

    @JsonProperty("name")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("campus")
    public String getCampus() {
        return campus;
    }

    @JsonProperty("campus")
    public void setCampus(String campus) {
        this.campus = campus;
    }

    @JsonProperty("alternative")
    public String getAlternative() {
        return alternative;
    }

    @JsonProperty("alternative")
    public void setAlternative(String alternative) {
        this.alternative = alternative;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("advice")
    public String getAdvice() {
        return advice;
    }

    @JsonProperty("advice")
    public void setAdvice(String advice) {
        this.advice = advice;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("image")
    public String getImageLink() {
        return imageLink;
    }

    @JsonProperty("image")
    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
    //endregion

    //region parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.code);
        dest.writeString(this.campus);
        dest.writeString(this.alternative);
        dest.writeString(this.description);
        dest.writeString(this.advice);
        dest.writeValue(this.longitude);
        dest.writeValue(this.latitude);
        dest.writeString(this.imageLink);
    }

    protected LocationModel(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.code = in.readString();
        this.campus = in.readString();
        this.alternative = in.readString();
        this.description = in.readString();
        this.advice = in.readString();
        this.longitude = (Double) in.readValue(Double.class.getClassLoader());
        this.latitude = (Double) in.readValue(Double.class.getClassLoader());
        this.imageLink = in.readString();
    }

    public static final Parcelable.Creator<LocationModel> CREATOR = new Parcelable.Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel source) {
            return new LocationModel(source);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };
    //endregion
}
