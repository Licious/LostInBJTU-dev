package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "southwest",
        "northeast"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class BoundsModel implements Parcelable {

    @JsonProperty("southwest")
    private LatitudeLongitudeModel southwest;
    @JsonProperty("northeast")
    private LatitudeLongitudeModel northeast;

    /**
     * No args constructor for use in serialization
     *
     */
    public BoundsModel() {
    }

    /**
     *
     * @param southwest
     * @param northeast
     */
    public BoundsModel(LatitudeLongitudeModel southwest,LatitudeLongitudeModel northeast) {
        super();
        this.southwest = southwest;
        this.northeast = northeast;
    }

    @JsonProperty("southwest")
    public LatitudeLongitudeModel getSouthwest() {
        return southwest;
    }

    @JsonProperty("southwest")
    public void setSouthwest(LatitudeLongitudeModel southwest) {
        this.southwest = southwest;
    }

    public BoundsModel withSouthwest(LatitudeLongitudeModel southwest) {
        this.southwest = southwest;
        return this;
    }

    @JsonProperty("northeast")
    public LatitudeLongitudeModel getNortheast() {
        return northeast;
    }

    @JsonProperty("northeast")
    public void setNortheast(LatitudeLongitudeModel northeast) {
        this.northeast = northeast;
    }

    public BoundsModel withNortheast(LatitudeLongitudeModel northeast) {
        this.northeast = northeast;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.southwest, flags);
        dest.writeParcelable(this.northeast, flags);
    }

    protected BoundsModel(Parcel in) {
        this.southwest = in.readParcelable(LatitudeLongitudeModel.class.getClassLoader());
        this.northeast = in.readParcelable(LatitudeLongitudeModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<BoundsModel> CREATOR = new Parcelable.Creator<BoundsModel>() {
        @Override
        public BoundsModel createFromParcel(Parcel source) {
            return new BoundsModel(source);
        }

        @Override
        public BoundsModel[] newArray(int size) {
            return new BoundsModel[size];
        }
    };
}
