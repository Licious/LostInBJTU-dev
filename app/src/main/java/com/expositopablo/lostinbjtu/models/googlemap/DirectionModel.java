package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "geocoded_waypoints",
        "routes"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DirectionModel implements Parcelable {

    @JsonProperty("status")
    private String status;
    @JsonProperty("geocoded_waypoints")
    private List<GeoCodedWaypoint> geocodedWaypoints = new ArrayList<GeoCodedWaypoint>();
    @JsonProperty("routes")
    private List<RouteModel> routes = new ArrayList<RouteModel>();

    /**
     * No args constructor for use in serialization
     *
     */
    public DirectionModel() {
    }

    /**
     *
     * @param routes
     * @param status
     * @param geocodedWaypoints
     */
    public DirectionModel(String status, List<GeoCodedWaypoint> geocodedWaypoints, List<RouteModel> routes) {
        super();
        this.status = status;
        this.geocodedWaypoints = geocodedWaypoints;
        this.routes = routes;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public DirectionModel withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("geocoded_waypoints")
    public List<GeoCodedWaypoint> getGeoCodedWaypoints() {
        return geocodedWaypoints;
    }

    @JsonProperty("geocoded_waypoints")
    public void setGeoCodedWaypoints(List<GeoCodedWaypoint> geocodedWaypoints) {
        this.geocodedWaypoints = geocodedWaypoints;
    }

    public DirectionModel withGeoCodedWaypoints(List<GeoCodedWaypoint> geocodedWaypoints) {
        this.geocodedWaypoints = geocodedWaypoints;
        return this;
    }

    @JsonProperty("routes")
    public List<RouteModel> getRouteModels() {
        return routes;
    }

    @JsonProperty("routes")
    public void setRouteModels(List<RouteModel> routes) {
        this.routes = routes;
    }

    public DirectionModel withRouteModels(List<RouteModel> routes) {
        this.routes = routes;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeTypedList(this.geocodedWaypoints);
        dest.writeList(this.routes);
    }

    protected DirectionModel(Parcel in) {
        this.status = in.readString();
        this.geocodedWaypoints = in.createTypedArrayList(GeoCodedWaypoint.CREATOR);
        this.routes = new ArrayList<RouteModel>();
        in.readList(this.routes, RouteModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<DirectionModel> CREATOR = new Parcelable.Creator<DirectionModel>() {
        @Override
        public DirectionModel createFromParcel(Parcel source) {
            return new DirectionModel(source);
        }

        @Override
        public DirectionModel[] newArray(int size) {
            return new DirectionModel[size];
        }
    };
}