package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "value",
        "text"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DurationModel implements Parcelable {

    @JsonProperty("value")
    private Integer value;
    @JsonProperty("text")
    private String text;

    /**
     * No args constructor for use in serialization
     *
     */
    public DurationModel() {
    }

    /**
     *
     * @param text
     * @param value
     */
    public DurationModel(Integer value, String text) {
        super();
        this.value = value;
        this.text = text;
    }

    @JsonProperty("value")
    public Integer getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Integer value) {
        this.value = value;
    }

    public DurationModel withValue(Integer value) {
        this.value = value;
        return this;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    public DurationModel withText(String text) {
        this.text = text;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.value);
        dest.writeString(this.text);
    }

    protected DurationModel(Parcel in) {
        this.value = (Integer) in.readValue(Integer.class.getClassLoader());
        this.text = in.readString();
    }

    public static final Parcelable.Creator<DurationModel> CREATOR = new Parcelable.Creator<DurationModel>() {
        @Override
        public DurationModel createFromParcel(Parcel source) {
            return new DurationModel(source);
        }

        @Override
        public DurationModel[] newArray(int size) {
            return new DurationModel[size];
        }
    };
}
