package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "geocoder_status",
        "place_id",
        "types"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoCodedWaypoint implements Parcelable{

    //region Attributes
    @JsonProperty("geocoder_status")
    private String geocoderStatus;
    @JsonProperty("place_id")
    private String placeId;
    @JsonProperty("types")
    private List<String> types = new ArrayList<>();
    //endregion

    //region Constructors
    /**
     * No args constructor for use in serialization
     *
     */
    public GeoCodedWaypoint() {
    }

    /**
     *
     * @param geocoderStatus
     * @param placeId
     * @param types
     */
    public GeoCodedWaypoint(String geocoderStatus, String placeId, List<String> types) {
        super();
        this.geocoderStatus = geocoderStatus;
        this.placeId = placeId;
        this.types = types;
    }
    //endregion

    //region Getter && Setter
    @JsonProperty("geocoder_status")
    public String getGeocoderStatus() {
        return geocoderStatus;
    }

    @JsonProperty("geocoder_status")
    public void setGeocoderStatus(String geocoderStatus) {
        this.geocoderStatus = geocoderStatus;
    }

    public GeoCodedWaypoint withGeocoderStatus(String geocoderStatus) {
        this.geocoderStatus = geocoderStatus;
        return this;
    }

    @JsonProperty("place_id")
    public String getPlaceId() {
        return placeId;
    }

    @JsonProperty("place_id")
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public GeoCodedWaypoint withPlaceId(String placeId) {
        this.placeId = placeId;
        return this;
    }

    @JsonProperty("types")
    public List<String> getTypes() {
        return types;
    }

    @JsonProperty("types")
    public void setTypes(List<String> types) {
        this.types = types;
    }

    public GeoCodedWaypoint withTypes(List<String> types) {
        this.types = types;
        return this;
    }
    //endregion

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.geocoderStatus);
        dest.writeString(this.placeId);
        dest.writeStringList(this.types);
    }

    protected GeoCodedWaypoint(Parcel in) {
        this.geocoderStatus = in.readString();
        this.placeId = in.readString();
        this.types = in.createStringArrayList();
    }

    public static final Creator<GeoCodedWaypoint> CREATOR = new Creator<GeoCodedWaypoint>() {
        @Override
        public GeoCodedWaypoint createFromParcel(Parcel source) {
            return new GeoCodedWaypoint(source);
        }

        @Override
        public GeoCodedWaypoint[] newArray(int size) {
            return new GeoCodedWaypoint[size];
        }
    };

    //endregion
}
