package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "lat",
        "lng"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LatitudeLongitudeModel implements Parcelable {

    @JsonProperty("lat")
    private Double lat;
    @JsonProperty("lng")
    private Double lng;

    /**
     * No args constructor for use in serialization
     *
     */
    public LatitudeLongitudeModel() {
    }

    /**
     *
     * @param lng
     * @param lat
     */
    public LatitudeLongitudeModel(Double lat, Double lng) {
        super();
        this.lat = lat;
        this.lng = lng;
    }

    @JsonProperty("lat")
    public Double getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Double lat) {
        this.lat = lat;
    }

    public LatitudeLongitudeModel withLat(Double lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lng")
    public Double getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Double lng) {
        this.lng = lng;
    }

    public LatitudeLongitudeModel withLng(Double lng) {
        this.lng = lng;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.lat);
        dest.writeValue(this.lng);
    }

    protected LatitudeLongitudeModel(Parcel in) {
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lng = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Parcelable.Creator<LatitudeLongitudeModel> CREATOR = new Parcelable.Creator<LatitudeLongitudeModel>() {
        @Override
        public LatitudeLongitudeModel createFromParcel(Parcel source) {
            return new LatitudeLongitudeModel(source);
        }

        @Override
        public LatitudeLongitudeModel[] newArray(int size) {
            return new LatitudeLongitudeModel[size];
        }
    };
}