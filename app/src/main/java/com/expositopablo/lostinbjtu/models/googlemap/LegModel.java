package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "steps",
        "start_address",
        "end_address",
        "distance",
        "duration",
        "end_location",
        "start_location",
        "traffic_speed_entry",
        "via_waypoint"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class LegModel implements Parcelable {

    @JsonProperty("steps")
    private List<StepModel> steps = new ArrayList<StepModel>();
    @JsonProperty("start_address")
    private String startAddress;
    @JsonProperty("end_address")
    private String endAddress;
    @JsonProperty("distance")
    private DistanceModel distance;
    @JsonProperty("duration")
    private DurationModel duration;
    @JsonProperty("end_location")
    private LatitudeLongitudeModel endLocation;
    @JsonProperty("start_location")
    private LatitudeLongitudeModel startLocation;
    /**
     * No args constructor for use in serialization
     *
     */
    public LegModel() {
    }

    /**
     *
     * @param startAddress
     * @param steps
     * @param endAddress
     * @param distance
     */
    public LegModel(List<StepModel> steps, String startAddress, String endAddress, DistanceModel distance, DurationModel durationModel, LatitudeLongitudeModel startLocation, LatitudeLongitudeModel endLocation) {
        super();
        this.steps = steps;
        this.startAddress = startAddress;
        this.endAddress = endAddress;
        this.distance = distance;
        this.duration = durationModel;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
    }

    @JsonProperty("steps")
    public List<StepModel> getStepModels() {
        return steps;
    }

    @JsonProperty("steps")
    public void setStepModels(List<StepModel> steps) {
        this.steps = steps;
    }

    public LegModel withStepModels(List<StepModel> steps) {
        this.steps = steps;
        return this;
    }

    @JsonProperty("start_address")
    public String getStartAddress() {
        return startAddress;
    }

    @JsonProperty("start_address")
    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public LegModel withStartAddress(String startAddress) {
        this.startAddress = startAddress;
        return this;
    }

    @JsonProperty("end_address")
    public String getEndAddress() {
        return endAddress;
    }

    @JsonProperty("end_address")
    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public LegModel withEndAddress(String endAddress) {
        this.endAddress = endAddress;
        return this;
    }

    @JsonProperty("distance")
    public DistanceModel getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(DistanceModel distance) {
        this.distance = distance;
    }

    @JsonProperty("duration")
    public DurationModel getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(DurationModel duration) {
        this.duration = duration;
    }

    @JsonProperty("end_location")
    public LatitudeLongitudeModel getEndLocation() {
        return endLocation;
    }

    @JsonProperty("end_location")
    public void setEndLocation(LatitudeLongitudeModel endLocation) {
        this.endLocation = endLocation;
    }

    @JsonProperty("start_location")
    public LatitudeLongitudeModel getStartLocation() {
        return startLocation;
    }

    @JsonProperty("start_location")
    public void setStartLocation(LatitudeLongitudeModel startLocation) {
        this.startLocation = startLocation;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.steps);
        dest.writeString(this.startAddress);
        dest.writeString(this.endAddress);
        dest.writeParcelable(this.distance, flags);
        dest.writeParcelable(this.duration, flags);
        dest.writeParcelable(this.endLocation, flags);
        dest.writeParcelable(this.startLocation, flags);
    }

    protected LegModel(Parcel in) {
        this.steps = in.createTypedArrayList(StepModel.CREATOR);
        this.startAddress = in.readString();
        this.endAddress = in.readString();
        this.distance = in.readParcelable(DistanceModel.class.getClassLoader());
        this.duration = in.readParcelable(DurationModel.class.getClassLoader());
        this.endLocation = in.readParcelable(LatitudeLongitudeModel.class.getClassLoader());
        this.startLocation = in.readParcelable(LatitudeLongitudeModel.class.getClassLoader());
    }

    public static final Creator<LegModel> CREATOR = new Creator<LegModel>() {
        @Override
        public LegModel createFromParcel(Parcel source) {
            return new LegModel(source);
        }

        @Override
        public LegModel[] newArray(int size) {
            return new LegModel[size];
        }
    };
}
