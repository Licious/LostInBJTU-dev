package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "points"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PolylineModel implements Parcelable {

    @JsonProperty("points")
    private String points;

    /**
     * No args constructor for use in serialization
     *
     */
    public PolylineModel() {
    }

    /**
     *
     * @param points
     */
    public PolylineModel(String points) {
        super();
        this.points = points;
    }

    @JsonProperty("points")
    public String getPoints() {
        return points;
    }

    @JsonProperty("points")
    public void setPoints(String points) {
        this.points = points;
    }

    public PolylineModel withPoints(String points) {
        this.points = points;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.points);
    }

    protected PolylineModel(Parcel in) {
        this.points = in.readString();
    }

    public static final Parcelable.Creator<PolylineModel> CREATOR = new Parcelable.Creator<PolylineModel>() {
        @Override
        public PolylineModel createFromParcel(Parcel source) {
            return new PolylineModel(source);
        }

        @Override
        public PolylineModel[] newArray(int size) {
            return new PolylineModel[size];
        }
    };
}
