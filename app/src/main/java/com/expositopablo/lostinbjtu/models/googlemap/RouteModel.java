package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "summary",
        "legs",
        "copyrights",
        "overview_polyline",
        "warnings",
        "waypoint_order",
        "bounds"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteModel implements Parcelable {

    @JsonProperty("summary")
    private String summary;
    @JsonProperty("legs")
    private List<LegModel> legs = new ArrayList<LegModel>();
    @JsonProperty("copyrights")
    private String copyrights;
    @JsonProperty("overview_polyline")
    private PolylineModel overviewPolyline;
    @JsonProperty("warnings")
    private List<String> warnings = new ArrayList<String>();
    @JsonProperty("waypoint_order")
    private List<Integer> waypointOrder = new ArrayList<Integer>();
    @JsonProperty("bounds")
    private BoundsModel bounds;

    /**
     * No args constructor for use in serialization
     *
     */
    public RouteModel() {
    }

    /**
     *
     * @param bounds
     * @param waypointOrder
     * @param summary
     * @param copyrights
     * @param legs
     * @param warnings
     * @param overviewPolyline
     */
    public RouteModel(String summary, List<LegModel> legs, String copyrights, PolylineModel overviewPolyline, List<String> warnings, List<Integer> waypointOrder, BoundsModel bounds) {
        super();
        this.summary = summary;
        this.legs = legs;
        this.copyrights = copyrights;
        this.overviewPolyline = overviewPolyline;
        this.warnings = warnings;
        this.waypointOrder = waypointOrder;
        this.bounds = bounds;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public RouteModel withSummary(String summary) {
        this.summary = summary;
        return this;
    }

    @JsonProperty("legs")
    public List<LegModel> getLegModels() {
        return legs;
    }

    @JsonProperty("legs")
    public void setLegModels(List<LegModel> legs) {
        this.legs = legs;
    }

    public RouteModel withLegModels(List<LegModel> legs) {
        this.legs = legs;
        return this;
    }

    @JsonProperty("copyrights")
    public String getCopyrights() {
        return copyrights;
    }

    @JsonProperty("copyrights")
    public void setCopyrights(String copyrights) {
        this.copyrights = copyrights;
    }

    public RouteModel withCopyrights(String copyrights) {
        this.copyrights = copyrights;
        return this;
    }

    @JsonProperty("overview_polyline")
    public PolylineModel getOverviewPolyline() {
        return overviewPolyline;
    }

    @JsonProperty("overview_polyline")
    public void setOverviewPolyline(PolylineModel overviewPolyline) {
        this.overviewPolyline = overviewPolyline;
    }

    public RouteModel withOverviewPolyline(PolylineModel overviewPolyline) {
        this.overviewPolyline = overviewPolyline;
        return this;
    }

    @JsonProperty("warnings")
    public List<String> getWarnings() {
        return warnings;
    }

    @JsonProperty("warnings")
    public void setWarnings(List<String> warnings) {
        this.warnings = warnings;
    }

    public RouteModel withWarnings(List<String> warnings) {
        this.warnings = warnings;
        return this;
    }

    @JsonProperty("waypoint_order")
    public List<Integer> getWaypointOrder() {
        return waypointOrder;
    }

    @JsonProperty("waypoint_order")
    public void setWaypointOrder(List<Integer> waypointOrder) {
        this.waypointOrder = waypointOrder;
    }

    public RouteModel withWaypointOrder(List<Integer> waypointOrder) {
        this.waypointOrder = waypointOrder;
        return this;
    }

    @JsonProperty("bounds")
    public BoundsModel getBoundsModel() {
        return bounds;
    }

    @JsonProperty("bounds")
    public void setBoundsModel(BoundsModel bounds) {
        this.bounds = bounds;
    }

    public RouteModel withBoundsModel(BoundsModel bounds) {
        this.bounds = bounds;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.summary);
        dest.writeTypedList(this.legs);
        dest.writeString(this.copyrights);
        dest.writeParcelable(this.overviewPolyline, flags);
        dest.writeStringList(this.warnings);
        dest.writeList(this.waypointOrder);
        dest.writeParcelable(this.bounds, flags);
    }

    protected RouteModel(Parcel in) {
        this.summary = in.readString();
        this.legs = in.createTypedArrayList(LegModel.CREATOR);
        this.copyrights = in.readString();
        this.overviewPolyline = in.readParcelable(PolylineModel.class.getClassLoader());
        this.warnings = in.createStringArrayList();
        this.waypointOrder = new ArrayList<Integer>();
        in.readList(this.waypointOrder, Integer.class.getClassLoader());
        this.bounds = in.readParcelable(BoundsModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<RouteModel> CREATOR = new Parcelable.Creator<RouteModel>() {
        @Override
        public RouteModel createFromParcel(Parcel source) {
            return new RouteModel(source);
        }

        @Override
        public RouteModel[] newArray(int size) {
            return new RouteModel[size];
        }
    };
}