package com.expositopablo.lostinbjtu.models.googlemap;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "travel_mode",
        "start_location",
        "end_location",
        "polyline",
        "duration",
        "html_instructions",
        "distance",
        "maneuver"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class StepModel implements Parcelable {

    @JsonProperty("travel_mode")
    private String travelMode;
    @JsonProperty("start_location")
    private LatitudeLongitudeModel startLocation;
    @JsonProperty("end_location")
    private LatitudeLongitudeModel endLocation;
    @JsonProperty("polyline")
    private PolylineModel polyline;
    @JsonProperty("duration")
    private DurationModel duration;
    @JsonProperty("html_instructions")
    private String htmlInstructions;
    @JsonProperty("distance")
    private DistanceModel distance;
    @JsonProperty("maneuver")
    private String maneuver;

    /**
     * No args constructor for use in serialization
     *
     */
    public StepModel() {
    }

    /**
     *
     * @param distance
     * @param duration
     * @param polyline
     * @param endLocation
     * @param htmlInstructions
     * @param startLocation
     * @param travelMode
     * @param maneuver
     */
    public StepModel(String travelMode, LatitudeLongitudeModel startLocation, LatitudeLongitudeModel endLocation, PolylineModel polyline, DurationModel duration, String htmlInstructions, DistanceModel distance, String maneuver) {
        super();
        this.travelMode = travelMode;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.polyline = polyline;
        this.duration = duration;
        this.htmlInstructions = htmlInstructions;
        this.distance = distance;
        this.maneuver = maneuver;
    }

    @JsonProperty("travel_mode")
    public String getTravelMode() {
        return travelMode;
    }

    @JsonProperty("travel_mode")
    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public StepModel withTravelMode(String travelMode) {
        this.travelMode = travelMode;
        return this;
    }

    @JsonProperty("start_location")
    public LatitudeLongitudeModel getStartLocation() {
        return startLocation;
    }

    @JsonProperty("start_location")
    public void setStartLocation(LatitudeLongitudeModel startLocation) {
        this.startLocation = startLocation;
    }

    public StepModel withstartLocation(LatitudeLongitudeModel startLocation) {
        this.startLocation = startLocation;
        return this;
    }

    @JsonProperty("end_location")
    public LatitudeLongitudeModel getEndLocation() {
        return endLocation;
    }

    @JsonProperty("end_location")
    public void setEndLocation(LatitudeLongitudeModel endLocation) {
        this.endLocation = endLocation;
    }

    public StepModel withEndLocation(LatitudeLongitudeModel endLocation) {
        this.endLocation = endLocation;
        return this;
    }

    @JsonProperty("polyline")
    public PolylineModel getPolylineModel() {
        return polyline;
    }

    @JsonProperty("polyline")
    public void setPolylineModel(PolylineModel polyline) {
        this.polyline = polyline;
    }

    public StepModel withPolylineModel(PolylineModel polyline) {
        this.polyline = polyline;
        return this;
    }

    @JsonProperty("duration")
    public DurationModel getDurationModel() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDurationModel(DurationModel duration) {
        this.duration = duration;
    }

    public StepModel withDurationModel(DurationModel duration) {
        this.duration = duration;
        return this;
    }

    @JsonProperty("html_instructions")
    public String getHtmlInstructions() {
        return htmlInstructions;
    }

    @JsonProperty("html_instructions")
    public void setHtmlInstructions(String htmlInstructions) {
        this.htmlInstructions = htmlInstructions;
    }

    public StepModel withHtmlInstructions(String htmlInstructions) {
        this.htmlInstructions = htmlInstructions;
        return this;
    }

    @JsonProperty("distance")
    public DistanceModel getDistanceModel() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistanceModel(DistanceModel distance) {
        this.distance = distance;
    }

    public StepModel withDistanceModel(DistanceModel distance) {
        this.distance = distance;
        return this;
    }

    @JsonProperty("maneuver")
    public String getManeuver() {
        return maneuver;
    }

    @JsonProperty("maneuver")
    public void setManeuver(String maneuver) {
        this.maneuver = maneuver;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.travelMode);
        dest.writeParcelable(this.startLocation, flags);
        dest.writeParcelable(this.endLocation, flags);
        dest.writeParcelable(this.polyline, flags);
        dest.writeParcelable(this.duration, flags);
        dest.writeString(this.htmlInstructions);
        dest.writeParcelable(this.distance, flags);
        dest.writeString(this.maneuver);
    }

    protected StepModel(Parcel in) {
        this.travelMode = in.readString();
        this.startLocation = in.readParcelable(LatitudeLongitudeModel.class.getClassLoader());
        this.endLocation = in.readParcelable(LatitudeLongitudeModel.class.getClassLoader());
        this.polyline = in.readParcelable(PolylineModel.class.getClassLoader());
        this.duration = in.readParcelable(DurationModel.class.getClassLoader());
        this.htmlInstructions = in.readString();
        this.distance = in.readParcelable(DistanceModel.class.getClassLoader());
        this.maneuver = in.readString();
    }

    public static final Creator<StepModel> CREATOR = new Creator<StepModel>() {
        @Override
        public StepModel createFromParcel(Parcel source) {
            return new StepModel(source);
        }

        @Override
        public StepModel[] newArray(int size) {
            return new StepModel[size];
        }
    };
}
