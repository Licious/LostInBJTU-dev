package com.expositopablo.lostinbjtu.network;

import com.expositopablo.lostinbjtu.models.EventModel;
import com.expositopablo.lostinbjtu.models.LocationModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;


public interface LocationNetworkService {

    /**
     * HTTP Request
     * @return List of all Building in BJTU
     */
    @GET("api/locations")
    Single<List<LocationModel>> getLocations();

    /**
     * HTTP Request
     * @return List of onGoing Events organized by BJTU
     */
    @GET("api/events")
    Single<List<EventModel>> getEvents();
}

