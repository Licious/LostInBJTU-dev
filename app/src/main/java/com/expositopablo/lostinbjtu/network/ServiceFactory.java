package com.expositopablo.lostinbjtu.network;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ServiceFactory {

    /**
     * Retrofit Service Factory : Create different Service for each different URL
     * @param mclass Network Interface
     * @param endpoint Url
     * @param <T> Type
     * @return Network Service to make HTTP Request
     */
    public static <T> T createRetrofitService(final Class<T> mclass, final String endpoint){

        //2017-05-20T18:25:43.511Z
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.z");
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.setDateFormat(df);

        final Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return restAdapter.create(mclass);
    }
}