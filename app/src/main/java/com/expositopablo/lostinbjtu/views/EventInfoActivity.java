package com.expositopablo.lostinbjtu.views;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.expositopablo.lostinbjtu.MapsActivity;
import com.expositopablo.lostinbjtu.R;
import com.expositopablo.lostinbjtu.customlayout.CustomChipCloud;
import com.expositopablo.lostinbjtu.models.EventModel;
import com.expositopablo.lostinbjtu.models.LocationModel;
import com.google.android.flexbox.FlexboxLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;

public class EventInfoActivity extends AppCompatActivity {

    public static final String LOCATION = "location";
    @BindView(R.id.textView_eventInfo_Name)
    protected TextView name;
    @BindView(R.id.textView_eventInfo_Descript)
    protected TextView description;
    @BindView(R.id.imageView_eventInfo_image)
    protected ImageView imageView;
    @BindView(R.id.fab_eventInfo_tel) @Nullable
    protected FloatingActionButton tel;
    @BindView(R.id.fab_eventInfo_Mail) @Nullable
    protected FloatingActionButton mail;
    @BindView(R.id.fab_eventInfo_goMap)
    protected FloatingActionButton map;
    @BindView(R.id.imageView_eventInfo_fav)
    protected ImageView fav;
    @BindView(R.id.flexboxLayout_eventInfo_chipContainer)
    protected FlexboxLayout chipContainer;

    
    private EventModel event;
    private Boolean isFav = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_info);
        ButterKnife.bind(this);
        event = getIntent().getExtras().getParcelable("EVENT");
        if (event != null){
            name.setText(event.getTitle());
            description.setText(event.getDescription());
            String image = event.getImageThumbs();
            if (image != null) {
                Picasso.with(this).load(image).resize(300,300).error(this.getResources().getDrawable(R.drawable.th)).into(imageView);
            }
            else
                imageView.setImageResource(R.drawable.th);

            List<String> categories =event.getCategories();
            if (categories != null) {
                ChipCloudConfig config = new ChipCloudConfig()
                        .selectMode(ChipCloud.SelectMode.none)
                        .checkedChipColor(Color.parseColor("#ff4081"))
                        .uncheckedChipColor(Color.parseColor("#ff4081"))
                        .useInsetPadding(true);
                CustomChipCloud chipCloud = new CustomChipCloud(this, chipContainer, config);
                chipCloud.refresh();
                for (String competence : categories) {
                    chipCloud.addChip(competence);
                }
            }
        }
    }

    @OnClick(R.id.fab_eventInfo_goMap)
    public void startMap(){
        Intent intent = new Intent(EventInfoActivity.this, MapsActivity.class);
        LocationModel location = event.getLocation();
        if (location != null) {
            intent.putExtra(LOCATION, event.getLocation());
        }
        startActivity(intent);
    }

    @OnClick(R.id.fab_eventInfo_tel)
    public void call(){
    }

    @OnClick(R.id.fab_eventInfo_Mail)
    public void mail(){

    }

    @OnClick(R.id.imageView_eventInfo_fav)
    public void fav(){
        isFav = !isFav;
        fav.setImageResource((isFav)? R.drawable.ic_favorite : R.drawable.ic_favorite_border);
    }

    @Override
    protected void onPause() {
        super.onPause();
        createDeleteNotif();
    }

    private void createDeleteNotif() {
    }
}
