package com.expositopablo.lostinbjtu.views;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.expositopablo.lostinbjtu.BuildConfig;
import com.expositopablo.lostinbjtu.MapsActivity;
import com.expositopablo.lostinbjtu.R;
import com.expositopablo.lostinbjtu.models.EventModel;
import com.expositopablo.lostinbjtu.models.LocationModel;
import com.expositopablo.lostinbjtu.network.LocationNetworkService;
import com.expositopablo.lostinbjtu.network.ServiceFactory;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class EventListActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int DATE_DIALOG_ID = 0xff;
    private static final String TAG = "EventListActivity";
    @BindView(R.id.recyclerView_eventList_list)
    @Nullable
    ShimmerRecyclerView recyclerView;
    @BindView(R.id.editText_eventList_dateFrom)
    @Nullable
    EditText dateFrom;
    @BindView(R.id.editText_eventList_dateTo)
    @Nullable
    EditText dateTo;
    @BindView(R.id.imageView_eventList_filter)
    @Nullable
    ImageView filter;

    private EventAdapter adapter;
    private List<EventModel> models = new ArrayList<>();
    private List<EventModel> filteredList;
    private int pickedEditText;
    private boolean[] selected = {true, true, true, true, true, true};
    final String[] categories = new String[]{"sport", "culture", "music", "travel", "work", "food"};
    private String searchWord =  null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventlist);
        ButterKnife.bind(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new EventAdapter(this, null);
        recyclerView.setAdapter(adapter);
        recyclerView.showShimmerAdapter();
        filter.setOnClickListener(this);
        dateFrom.setOnClickListener(this);
        dateTo.setKeyListener(null);
        dateFrom.setKeyListener(null);
        dateTo.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startRequest();
    }

    private void startRequest() {
        Log.i(TAG, "startRequest");
        LocationNetworkService network = ServiceFactory.createRetrofitService(LocationNetworkService.class, BuildConfig.SERVER_ENDPOINT);
        Single<List<EventModel>> locationNetworkObservable = network.getEvents();
        locationNetworkObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(events -> {
                    if (events != null) {
                        Log.i(TAG, "Event Receive : " + events.size());
                        recyclerView.hideShimmerAdapter();
                        models = events;
                        adapter.updateEvent(events);
                        filterList(null);
                    }
                    else
                        Log.i(TAG, "Event Receive but null");
                }, throwable -> {
                    //Something Went Wrong ==> Toast user.
                    LocationModel location = new LocationModel(null, "YiFu Teaching Building", "YF", "Main Campus", "AlternativeCode", "Main building, in front of the big area of grass and chinese flag.", "First floor smell, bring some perfum, not a lot of plugs and wifi is horrible", 39.951135, 116.340892, "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Bjtu_siyuan_building.JPG/280px-Bjtu_siyuan_building.JPG");

                    Log.i(TAG, "on error : " + throwable.getMessage());
                    List<EventModel> list = new ArrayList();
                    List<String> workCat = new ArrayList<>();
                    workCat.add("work");
                    Calendar cal = Calendar.getInstance();
                    cal.set(2017, 05, 13, 0, 0, 0);
                    list.add(new EventModel("Water Great Wall", cal.getTime(), workCat, "Come with us enjoy a wonderfull afternoon at the Water Great Wall. Let's meet at 10am in front of the South Gate of Main Campus, before taking the bus for a 2hour trip. Back at the university for 4pm", "http://tse4.mm.bing.net/th?id=OIP.yq9FVKJ6J65JAAYQ_eWHbQEsCo&pid=15.1", "http://tse4.mm.bing.net/th?id=OIP.yq9FVKJ6J65JAAYQ_eWHbQEsCo&pid=15.1", "16129132@bjtu.edu.cn", "+8613121513959", location));
                    workCat = new ArrayList<String>();
                    workCat.add("Outdoor");
                    workCat.add("Sport");
                    workCat.add("culture");
                    cal.set(2017, 05, 13, 0, 0, 0);
                    list.add(new EventModel("Karaoke : KTV", cal.getTime(), workCat, "Bring your best diva impression... or not! Anyway we're here for fun no need to be a professional singer. Let's meet at 4pm in front of the South Gate of Main Campus, the KTV is only at 10 mn walk from the university", "http://tse3.mm.bing.net/th?id=OIP.C28WN7yUzNeUdFeJRbqR5gEsDq&pid=15.1", "http://tse3.mm.bing.net/th?id=OIP.C28WN7yUzNeUdFeJRbqR5gEsDq&pid=15.1", "16129132@bjtu.edu.cn", "+8613121513959", location));
                    workCat = new ArrayList<String>();
                    workCat.add("Outdoor");
                    workCat.add("music");
                    workCat.add("culture");
                    cal.set(2017, 05, 13, 0, 0, 0);
                    list.add(new EventModel("Jackass!", cal.getTime(), workCat, "If you need some danger, some excitement, come meet us for a crazy ride in Bike, rollerblade or skate. Let's meet in front of the South Gate at 8pm, bring your favorites ride with you. (NO PONEY!)", "http://tse3.mm.bing.net/th?id=OIP.C28WN7yUzNeUdFeJRbqR5gEsDq&pid=15.1", "http://tse2.mm.bing.net/th?id=OIP.CUMPd6B1sYpYc38_7mnSZAEnDT&pid=15.1", "http://tse2.mm.bing.net/th?id=OIP.CUMPd6B1sYpYc38_7mnSZAEnDT&pid=15.1", "+8613121513959", location));
                    workCat = new ArrayList<String>();
                    workCat.add("Outdoor");
                    workCat.add("sport");
                    cal.set(2017, 05, 20, 0, 0, 0);
                    list.add(new EventModel("Project Training Final Report", cal.getTime(), workCat, "Last Day to defent your project and get a good grade!", "https://tse2-mm.cn.bing.net/th?id=OIP.Kx-jXWB9J_9Houl6crXzcgEsC5&w=300&h=300&p=0&pid=1.1", "https://tse2-mm.cn.bing.net/th?id=OIP.Kx-jXWB9J_9Houl6crXzcgEsC5&w=300&h=300&p=0&pid=1.1", "16129132@bjtu.edu.cn", "+8613121513959", location));
                    workCat = new ArrayList<String>();
                    workCat.add("music");
                    cal.set(2017, 05, 21, 0, 0, 0);
                    list.add(new EventModel("Everybody do the flop!", cal.getTime(), workCat, "Well... everybody do the flop", "https://tse4-mm.cn.bing.net/th?id=OIP.Ruik4PWBpywJINSkqQreTgEsCo&w=300&h=300&p=0&pid=1.1", "https://tse4-mm.cn.bing.net/th?id=OIP.Ruik4PWBpywJINSkqQreTgEsCo&w=300&h=300&p=0&pid=1.1", "16129132@bjtu.edu.cn", "+8613121513959", location));
                    cal.set(2017, 10, 10, 0, 0, 0);
                    list.add(new EventModel("Leek Spin", cal.getTime(), workCat, "lapapapapapapaap", "http://i1.kym-cdn.com/photos/images/original/000/146/769/Memey.GIF", "http://i1.kym-cdn.com/photos/images/original/000/146/769/Memey.GIF", "16129132@bjtu.edu.cn", "+8613121513959", location));
                    recyclerView.hideShimmerAdapter();
                    models = list;
                    //adapter.updateEvent(list);
                    filterList(null);
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        recyclerView.showShimmerAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_events, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchWord = query;
                filterList(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchWord = null;
                filterList(null);
                return false;
            }
        });

        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageView_eventList_filter:
                showCategoriesPicker();
                break;
            case R.id.editText_eventList_dateFrom:
                pickedEditText = R.id.editText_eventList_dateFrom;
                showDatePicker();
                break;
            case R.id.editText_eventList_dateTo:
                pickedEditText = R.id.editText_eventList_dateTo;
                showDatePicker();
                break;
            default:
                break;
        }
    }

    public void showDatePicker() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void showCategoriesPicker() {
        final AlertDialog.Builder build = new AlertDialog.Builder(this);
        build.setTitle("Categories selection");
        build.setCancelable(true);
        final DialogInterface.OnMultiChoiceClickListener onClick =
                (dialog, which, isChecked) ->
                    selected[which] = isChecked;
        build.setMultiChoiceItems(categories, selected, onClick);
        build.setPositiveButton("Done", (dialog, which) -> {
            filterList(null);
        });
        build.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        switch (pickedEditText) {
            case R.id.editText_eventList_dateFrom:
                dateFrom.setText(day + "/" + (month + 1) + "/" + year);
                pickedEditText = 0;
                break;
            case R.id.editText_eventList_dateTo:
                dateTo.setText(day + "/" + (month + 1) + "/" + year);
                pickedEditText = 0;
                break;
            default:
                break;
        }
        filterList(null);
    }

    private void filterList(String query) {
        filteredList = new ArrayList<>();
        recyclerView.showShimmerAdapter();

        EventModel[] modelArray = models.toArray(new EventModel[models.size()]);
        Observable<EventModel> observable = Observable.fromArray(modelArray);

        if (dateFrom != null && !dateFrom.getEditableText().toString().isEmpty()) {
            Date from = getDateFromEditText(dateFrom, -1);
            Log.i(TAG, from.toString());
            observable = observable.filter(event -> {
                        Log.i(TAG, "event Date : " + event.getDate().toString());
                        return event.getDate().after(from);
            });
        }

        if (dateTo != null && !dateTo.getEditableText().toString().isEmpty()) {
            Date to = getDateFromEditText(dateTo, +1);
            Log.i(TAG, to.toString());
            observable = observable.filter(event ->{
                Log.i(TAG, "event Date : " + event.getDate().toString());
                return event.getDate().before(to);
            });
        }

        List<String> categorieFilter = new ArrayList<>();
        for (int i = 0; i < categories.length; i++){
            if (selected[i])
                categorieFilter.add(categories[i]);
        }
        observable = observable.filter(event -> !Collections.disjoint(event.getCategories(), categorieFilter));

        if (searchWord != null && !searchWord.isEmpty())
            observable = observable.filter(eventModel -> eventModel.getTitle().toLowerCase().contains(searchWord.toLowerCase()));

        observable.subscribe(new Observer<EventModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(EventModel value) {
                filteredList.add(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                adapter.updateEvent(filteredList);
                recyclerView.hideShimmerAdapter();
            }
        });

    }

    private Date getDateFromEditText(EditText editText, int addDay){
        Calendar cal = Calendar.getInstance();
        if (editText != null && !editText.getEditableText().toString().isEmpty()) {
            String[] dateSplit = editText.getEditableText().toString().split("/");
            Log.i(TAG, "getDateFromEditText - dateSplit : " + dateSplit[2] + "/" + dateSplit[1] + "/" + dateSplit[0]);
            cal.set(Integer.parseInt(dateSplit[2]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[0]), 0, 0, 0);
            cal.add(Calendar.DATE, addDay);
        }
        return cal.getTime();
    }
    private class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder>{

        private final Context context;
        private List<EventModel> events;

        public EventAdapter(Context context, List<EventModel> events) {
            this.context = context;
            this.events = events;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_events, parent, false);
            return new ViewHolder(view, context);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.bind(events.get(position));
        }

        @Override
        public int getItemCount() {
            return (events != null)? events.size():0;
        }

        public void updateEvent(List<EventModel> events) {
            this.events = events;
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            View view;
            ImageView thumbs;
            ImageView notif;
            TextView title;
            TextView categorie;
            TextView more;
            TextView month;
            TextView day;
            Context context;

            public ViewHolder(View itemView, Context context) {
                super(itemView);
                view = itemView;
                thumbs = (ImageView) itemView.findViewById(R.id.imageView_eventList_thumb);
                notif = (ImageView) itemView.findViewById(R.id.imageView_eventList_favorite);
                title = (TextView) itemView.findViewById(R.id.textView_eventList_activityName);
                categorie = (TextView) itemView.findViewById(R.id.textView_eventList_activityType);
                more = (TextView) itemView.findViewById(R.id.textView_eventList_more);
                month = (TextView) itemView.findViewById(R.id.textView_eventList_activityDate);
                day = (TextView) itemView.findViewById(R.id.textView5);
                this.context = context;
            }

            public void bind(EventModel locationModel) {
                more.setOnClickListener(view -> {
                    Intent intent = new Intent(context, EventInfoActivity.class);
                    intent.putExtra("EVENT", locationModel);
                    startActivity(intent);
                });
                notif.setOnClickListener(view -> {
                    //TODO : Create or destroy notif
                });
                if (thumbs != null)
                    Picasso.with(context).load(locationModel.getImageThumbs()).resize(300,300).error(context.getResources().getDrawable(R.drawable.th)).into(thumbs);
                else
                    thumbs.setImageResource(R.drawable.th);
                title.setText(locationModel.getTitle());
                if (categorie != null)
                    categorie.setText(locationModel.getCategories().toString().replace("[", "").replace("]", ""));
                Calendar cal = Calendar.getInstance();
                cal.setTime(locationModel.getDate());
                String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
                month.setText(months[cal.get(Calendar.MONTH)]);
                day.setText("" + cal.get(Calendar.DATE));
            }
        }
    }
}
