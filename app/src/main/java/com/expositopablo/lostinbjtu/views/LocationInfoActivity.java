package com.expositopablo.lostinbjtu.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.expositopablo.lostinbjtu.MapsActivity;
import com.expositopablo.lostinbjtu.R;
import com.expositopablo.lostinbjtu.models.LocationModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocationInfoActivity extends AppCompatActivity {

    @BindView(R.id.textView_locationDetails_campus) protected TextView campus;
    @BindView(R.id.textView_locationDetails_code) protected TextView code;
    @BindView(R.id.textView_locationDetails_descript) protected TextView description;
    @BindView(R.id.textView_locationDetail_advice) protected TextView advice;
    @BindView(R.id.imageView_locationImage) protected ImageView image;
    @BindView(R.id.textView_locationDetails_name) protected TextView name;
    @BindView(R.id.textView_locationDetails_alternative) protected TextView alternative;

    private LocationModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_info);
        ButterKnife.bind(this);
        model = getIntent().getExtras().getParcelable("location");
        if (model != null) {
            campus.setText(model.getCampus());
            code.setText(model.getCode());
            description.setText(model.getDescription());
            advice.setText(model.getAdvice());
            String alternativeString = model.getAlternative();
            if (alternativeString != null && !alternativeString.isEmpty())
                alternative.setText(alternativeString);
            else
                alternative.setVisibility(View.GONE);
            name.setText(model.getTitle());
            String imageLink = model.getImageLink();
            if (imageLink != null)
                Picasso.with(this).load(model.getImageLink()).resize(300,300).error(this.getResources().getDrawable(R.drawable.th)).into(image);
            else
                image.setImageResource(R.drawable.th);
        }
    }


    @OnClick(R.id.floatingActionButton)
    public void submitMessage()
    {
        Intent intentMessage=new Intent();
        intentMessage.putExtra("Return", model);
        setResult(MapsActivity.REQUEST_CODE,intentMessage);
        finish();
    }
}
